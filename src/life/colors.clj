(ns life.colors
  (:require [clojure.math.numeric-tower :as math]))

(defn int-rgb->components
  "Converts 0xRrGgBb to [0xRr 0xGg 0xBb]"
  [code]
  [(bit-shift-right (bit-and 0xFF0000 code) 16)
   (bit-shift-right (bit-and 0x00FF00 code) 8)
   (bit-and 0x0000FF code)])

(defn rgb-components->int
  "Converts [0xRr 0xGg 0xBb] to 0xRrGgBb"
  [[r g b]]
  (bit-or
    (bit-shift-left r 16)
    (bit-shift-left g 8)
    b))

(defn blend-colors
  "Blends [0xRr 0xGg 0xBb] colors"
  ([] '(0 0 0))
  ([colors]
   (map #(-> (reduce + %) (/ (count colors)) double math/round)
        (apply map vector colors))))

(defn dominanting-color
  "Blends [0xRr 0xGg 0xBb] colors"
  ([] '(0 0 0))
  ([colors]
   (key (last (sort-by val (frequencies colors))))))
(ns life.strconversions
  (:require
    [clojure.string :as str]))

(defn parse-rules
  "Parses S/B rulestring into {:survive, :birth} "
  [s]
  {:pre [(re-matches #"^B\d{0,9}\/S\d{0,9}|S\d{0,9}\/B\d{0,9}$" s)]}
  {:birth (map (comp read-string str) (nth (re-find #"B(\d{0,9})" s) 1))
   :survive (map (comp read-string str) (nth (re-find #"S(\d{0,9})" s) 1))})

(defn cells->str
  "Returns string representation of cells set"
  ([cells default-value value->str [minX maxX] [minY maxY]]
   (let [points (keys cells)]
     (str/join
       \newline
       (map (partial str/join \space)
            (for [y (range minY (inc maxY))]
              (for [x (range minX (inc maxX))]
                (value->str (get cells {:x x :y y} default-value))))))))

  ([cells default-value value->str]
   (let [points (keys cells)
         Xs (map :x points)
         Ys (map :y points)]
     (cells->str
       cells
       default-value
       value->str
       [(apply min Xs) (apply max Xs)]
       [(apply min Ys) (apply max Ys)]))))

(defn line->cells
  "Converts string with cells to sequence of maps of Points
  It is str->cells helper, so it requires y argument."
  [str->value y line]
  {:pre [(re-matches #"^((?:^| )(?:.+?)(?= |$))+$" line)]}
  (into {}
        (filter
          (fn [[{:keys [x y]} value]] value)
          (into {}
                (map-indexed
                  (fn [x value] {{:x x :y y} (when (not= "." value) (str->value value))})
                  (str/split line #" "))))))

(defn str->cells
  "Converts string with cells separated with
  delimiter and new lines to Points hash map"
  [s str->value]
  {:pre [(re-matches #"^((?:^|\s)(?:.+?)(?=\s|$))+$" s)]}
  (->> s
       (str/split-lines)
       (map-indexed (partial line->cells str->value))
       (flatten)
       (into {})))

;(str->cells "1 1\n1 1\n. . 1 1\n. . 1 1" str)
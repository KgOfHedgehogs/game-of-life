(ns life.algo
  (:require
    [clojure.core.reducers :as r]
    [life.misc :refer [group-by-first
                       contains-val?
                       random-rgb-cells
                       filter-by-val]]
    [life.point :refer [moore-neighborhood]]
    [life.colors :refer [blend-colors]]))

(defn group-by-first-up-to-n
  "Groups collection of [key &vals] by k, excluding k from pair, up to n vals per key"
  [n coll]
  (persistent!
    (reduce
      (fn [ret x]
        (let [k (first x)
              el (get ret k (list))]
          (if (< (count el) n)
            (assoc! ret k (concat el (rest x)))
            ret)))
      (transient {})
      coll)))

(defn find-neighbors
  "Returns cell and its neighbors values.
  n is the maximum number of neighbors that makes sense."
  [cells n]
  (group-by-first-up-to-n
    n
    (mapcat (fn [[point value]]
              (map #(list % value)
                   (moore-neighborhood point)))
            cells)))

(defn alive-cell-filter
  [world rules]
  (filter
    (fn [[point values]]
      (let [cnt (count values)
            alive (contains? world point)]
        (or (and (not alive)
                 (contains-val? (:birth rules) cnt))
            (and alive
                 (contains-val? (:survive rules) cnt)))))))

(defn range-cell-filter
  [[minX maxX] [minY maxY]]
  (filter
    (fn [[{:keys [x y]} value]]
      (and (<= minX x maxX) (<= minY y maxY)))))

(defn handle-cells-map
  [world value-handler]
  (map
    (fn [[point values]]
      {point (get world point (value-handler values))})))

(defn game-step
  "Conway's game of life iteration."
  ([rules value-handler world]
   (transduce
     (comp
       (alive-cell-filter world rules)
       (handle-cells-map world value-handler))
     conj
     {}
     (find-neighbors
       world
       (inc (transduce (mapcat val) max 0 rules)))))

  ([rules value-handler rngX rngY world]
   (transduce
     (comp
       (range-cell-filter rngX rngY)
       (alive-cell-filter world rules)
       (handle-cells-map world value-handler))
     conj
     {}
     (find-neighbors
       world
       (inc (transduce (mapcat val) max 0 rules))))))

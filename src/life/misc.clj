(ns life.misc
  (:require [clojure.string :as str]
            [clojure.math.numeric-tower :as math]
            [clojure.core.reducers :as r]
            [life.colors :refer [int-rgb->components
                                 rgb-components->int]])
  (:import (java.text DecimalFormat)))

(defn num->hex [num] (format "0x%06X" num))

(defn hex->num
  "Converts string with hex number to int"
  [s]
  {:pre [(re-matches #"0x[0-9A-F]+" s)]}
  (Integer/parseInt (.substring s 2) 16))

(def hex->rgb-components (comp int-rgb->components hex->num))
(def rgb-components->hex (comp num->hex rgb-components->int))

(defn float->round2str [x]
  (.format (new DecimalFormat "#.##") x))

(defn contains-val?
  [coll val]
  (r/reduce #(if (= val %2) (reduced true) %1) false coll))

(defn update-map [m f]
  (reduce-kv (fn [m k v] (assoc m k (f v))) {} m))

(defn filter-by-val
  [pred m]
  (persistent!
    (reduce-kv (fn [acc k v]
                 (if (pred v)
                   (conj! acc [k v])
                   acc))
               (transient {})
               m)))

(defn deep-merge [a b]
  (merge-with (fn [x y]
                (cond (map? y) (deep-merge x y)
                      (vector? y) (concat x y)
                      :else y))
              a b))

(defn group-by-first
  "Groups coll of seqs by first element of each seq"
  [coll]
  (as-> (group-by first coll) coll
        (zipmap
          (keys coll)
          (map (partial map last)
               (vals coll)))))

(defn recolor-cells [new-color cells]
  (zipmap (keys cells) (repeat new-color)))

(defn random-rgb-cells
  [[minX maxX] [minY maxY]]
  (let [width (- maxX minX)
        height (- maxY minY)
        colors [[255 0 0] [0 255 0] [0 0 255]]]
    (into {}
          (for [_ (range (* width height))
                :when (rand-nth [false false false true])]
            {{:x (math/round (+ (rand-int width) minX))
              :y (math/round (+ (rand-int height) minY))}
             (rand-nth colors)}))))

(defn rect-cells
  [x y width height col]
  (into {}
    (let [step-y (if (pos? height) 1 -1)
          step-x (if (pos? width) 1 -1)]
      (for [j (range y (+ y height step-y) step-y)
            i (range x (+ x width step-x) step-x)]
        {{:x i, :y j} col}))))

(defn limited [l r f]
  "Returns function with result guaranteed to be between l and r inclusive"
  (fn [& args]
    (let [x (apply f args)]
      (cond
        (<= l x r) x
        (> x r) r
        (< x l) l))))



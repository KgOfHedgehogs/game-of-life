(ns life.figures
  (:require
    [life.strconversions :refer [str->cells]]
    [life.misc :refer [hex->rgb-components]]
    [life.point :refer [cells-offset
                        center-cells]]))

(def blinker
  (center-cells
    (str->cells
      "0xFF0000 0x0000FF 0xFF0000"
      hex->rgb-components)))
(def toad
  (center-cells
    (str->cells
      "0x00FF00 0x00FF00 0x00FF00\n. 0x00FF00 0x00FF00 0x00FF00"
      hex->rgb-components)))
(def beakon
  (center-cells
    (str->cells
      "0x7F7F00 0x7F7F00\n0x7F7F00 0x7F7F00\n. . 0x0000FF 0x0000FF\n. . 0x0000FF 0x0000FF"
      hex->rgb-components)))
(def eight
  (center-cells
    (str->cells
      "0x007F7F 0x007F7F 0x007F7F\n0x007F7F 0x007F7F 0x007F7F\n0x007F7F 0x007F7F 0x007F7F\n. . . 0xFF0000 0xFF0000 0xFF0000\n. . . 0xFF0000 0xFF0000 0xFF0000\n. . . 0xFF0000 0xFF0000 0xFF0000"
      hex->rgb-components)))
(def pentadecathlon
  (center-cells
    (str->cells
      ". . 0xFF00FF . . . . 0xFF00FF\n0xFFFFFF 0xFFFFFF . 0xFFFFFF 0x00FFFF 0x00FFFF 0xFFFFFF . 0xFFFFFF 0xFFFFFF\n. . 0xFFFF00 . . . . 0xFFFF00 "
      hex->rgb-components)))
(def pulsar
  (center-cells
    (str->cells
      ". . 0xFFFF00 0xFFFF00 0xFFFF00 . . . 0xFFFF00 0xFFFF00 0xFFFF00\n.\n0xFFFF00 . . . . 0xFF0000 . 0xFF0000 . . . . 0xFFFF00\n0xFFFF00 . . . . 0xFF0000 . 0xFF0000 . . . . 0xFFFF00\n0xFFFF00 . . . . 0xFF0000 . 0xFF0000 . . . . 0xFFFF00\n. . 0xFF0000 0xFF0000 0xFF0000 . . . 0xFF0000 0xFF0000 0xFF0000\n.\n. . 0xFF0000 0xFF0000 0xFF0000 . . . 0xFF0000 0xFF0000 0xFF0000\n0xFFFF00 . . . . 0xFF0000 . 0xFF0000 . . . . 0xFFFF00\n0xFFFF00 . . . . 0xFF0000 . 0xFF0000 . . . . 0xFFFF00\n0xFFFF00 . . . . 0xFF0000 . 0xFF0000 . . . . 0xFFFF00\n.\n. . 0xFFFF00 0xFFFF00 0xFFFF00 . . . 0xFFFF00 0xFFFF00 0xFFFF00"
      hex->rgb-components)))
(def diehard
  (center-cells
    (str->cells
      ". . . . . . 0x0000FF\n0x0000FF 0x0000FF\n. 0x0000FF . . . 0x00FF00 0x00FF00 0x00FF00"
      hex->rgb-components)))
(def rpentomino
  (center-cells
    (str->cells
      ". 0xFF0000 0x0000FF\n0x00FFFF 0x0000FF\n. 0xFF0000"
      hex->rgb-components)))

(def zoo
  (merge
    (cells-offset {:x 1 :y 2} blinker)
    (cells-offset {:x 1 :y 7} toad)
    (cells-offset {:x 1 :y 12} beakon)
    (cells-offset {:x 8 :y 2} pulsar)
    (cells-offset {:x 6 :y 22} pentadecathlon)
    (cells-offset {:x 7 :y 34} diehard)
    (cells-offset {:x 26 :y 3} eight)))
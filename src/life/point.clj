(ns life.point)

(defn point-offset
  "Returns delta offset of point"
  [{dx :x, dy :y} point]
  (merge-with + point {:x dx, :y dy}))

(defn cells-offset
  "Returns delta offset of cells set"
  [delta cells]
  (into {} (map (fn [[point value]] [(point-offset delta point) value])
                cells)))

(defn points-offset
  "Returns delta offset of points set"
  [delta points]
  (map (partial point-offset delta) points))

(defn center-cells
  "Centers cells"
  ([cells]
   (if (empty? cells)
     {}
     (let [points (keys cells)
           Xs (map :x points)
           Ys (map :y points)
           maxX (apply max Xs)
           maxY (apply max Ys)
           minX (apply min Xs)
           minY (apply min Ys)]
       (cells-offset
         {:x (- (- maxX (quot (- maxX minX) 2)))
          :y (- (- maxY (quot (- maxY minY) 2)))}
         cells)))))

(defn moore-neighborhood
  "Returns moore neighborhood for {:x 0, y:0}
  optionally with {:x dx, :y dy} offset"
  ([] (moore-neighborhood {:x 0 :y 0}))
  ([{:keys [x y]}]
   (let [x-- (dec x) x++ (inc x)
         y-- (dec y) y++ (inc y)]
     [{:x x-- :y y--} {:x x :y y--} {:x x++ :y y--}
      {:x x-- :y y} {:x x++ :y y}
      {:x x-- :y y++} {:x x :y y++} {:x x++ :y y++}])))
(ns life.core
  (:require
    [clojure.math.numeric-tower :as math]
    [clojure.string :as str]
    [clojure.edn :as edn]

    [quil.core :as q :include-macros true]
    [quil.middleware :as m]

    [life.mymiddleware :refer [record-gif
                               navigation-2d
                               show-info]]
    [life.point :refer [cells-offset
                        center-cells]]
    [life.colors :refer [blend-colors
                         dominanting-color]]
    [life.strconversions :refer [parse-rules]]
    [life.misc :refer [random-rgb-cells
                       recolor-cells
                       limited
                       rect-cells
                       update-map
                       float->round2str
                       deep-merge]]
    [life.algo :refer [game-step]]
    [life.figures :refer :all])
  (:gen-class))

;TODO:
; exception if settings.edn not found
; change title
; fix bug with ignoring 0 in rules (better to rewrite all algos)
; cljs
; speedup algos

(defn disp-coord->field-coord
  [state x y]
  (let [x (- x (/ (q/width) 2))
        y (- y (/ (q/height) 2))
        {zoom :zoom, [dx dy] :position} (:navigation-2d state)
        dx (- dx (/ (q/width) 2))
        dy (- dy (/ (q/height) 2))]
    {:x (int (math/floor (+ (/ x zoom) dx)))
     :y (int (math/floor (+ (/ y zoom) dy)))}))

(def default-settings
  {:field         {}
   :rules         {:birth   [3]
                   :survive [2 3]}
   :update-rate   12
   :paused        true
   :generation    0
   :last-render   0
   :key-bindings  {32 :pause
                   44 :dec-update-rate
                   46 :inc-update-rate
                   69 :reset-brush
                   82 :reset-camera
                   67 :toggle-grid
                   68 :toogle-nav-draw-mode
                   70 :toggle-show-frame-rate
                   71 :toggle-show-generation
                   72 :toggle-show-delta
                   74 :toggle-show-zoom}
   :grid          {:enabled true
                   :hidden  false}
   :show-info     {:target-frame-rate false
                   :update-rate       false
                   :generation        false
                   :delta             false
                   :zoom              false}
   :draw          {:enabled true
                   :pick    2
                   :palette [[255 255 255] [255 255 255] [255 255 255]
                             [255 0 0] [0 255 0] [0 0 255]]
                   :buffer  {{:x 0 :y 0} [255 255 255]}
                   :brush   {{:x 0 :y 0} [255 255 255]}}
   :navigation-2d {:enabled false
                   :zoom    5}})

(def user-settings
  (edn/read-string (slurp "settings.edn")))

(defn setup []
  (q/frame-rate 30)
  (q/background 0)
  (deep-merge default-settings user-settings))

(def info-provider
  {:update-rate       (fn [state] (state :update-rate))
   :target-frame-rate (fn [state] (float->round2str (q/current-frame-rate)))
   :generation        (fn [state] (state :generation))
   :delta             (fn [state] (str/join
                                    \space
                                    (map float->round2str
                                         (-> state :navigation-2d :position))))
   :zoom              (fn [state] (float->round2str
                                    (-> state :navigation-2d :zoom)))})

(defn update-state [state]
  (cond-> (assoc-in state [:grid :hidden] (< (-> state :navigation-2d :zoom) 5))

          (not (contains? state :reset-navigation))
          (assoc :reset-navigation (dissoc (state :navigation-2d) :enabled))

          (and (not (:paused state))
               (>= (- (q/millis) (:last-render state))
                   (/ 1000 (:update-rate state))))
          (-> (update :generation inc)
              (update :field (partial game-step (:rules state) blend-colors))
              (assoc :last-render (q/millis)))

          (empty? (state :field))
          (assoc :paused true)))

(defn draw-cells [cells]
  (q/no-stroke)
  (doseq [[{:keys [x y]} col] cells]
    (let [x (+ (/ (q/width) 2.0) x)
          y (+ (/ (q/height) 2.0) y)]
      (apply q/fill col)
      (q/rect x y 0.9 0.9))))

(defn draw-state [state]
  (if (and (-> state :grid :enabled) (not (-> state :grid :hidden)))
    (q/background 0x20 0x20 0x20)
    (q/background 0))
  (draw-cells
    (merge
      (when (and (-> state :grid :enabled) (not (-> state :grid :hidden)))
        (let [{x0 :x, y0 :y} (disp-coord->field-coord state 0 0)
              {x1 :x, y1 :y} (disp-coord->field-coord state (q/width) (q/height))]
          (rect-cells x0 y0 (inc (- x1 x0)) (inc (- y1 y0)) [0 0 0])))
      (:field state)))
  (let [{:keys [x0 y0 x1 y1]} (state :selection)]
    (cond
      (and x0 y0 x1 y1)
      (draw-cells (rect-cells x0 y0 (- x1 x0) (- y1 y0) [0xFF 0xFF 0xFF 0x50]))
      (get-in state [:draw :enabled])
      (let [coord (disp-coord->field-coord state (q/mouse-x) (q/mouse-y))
            brush (-> (-> state :draw :brush)
                      (update-map #(concat % [0xB0])))]
        (draw-cells (cells-offset coord brush))))))

(defn mouse-pressed [state event]
  (if (and (q/key-pressed?) (:control (q/key-modifiers)))
    (let [coords (disp-coord->field-coord state (q/mouse-x) (q/mouse-y))]
      (-> state
          (assoc-in [:selection :x0] (:x coords))
          (assoc-in [:selection :y0] (:y coords))
          (assoc :last-mouse-button (:button event))))
    (assoc state :last-mouse-button (:button event))))

(defn toggle-nav-draw-mode [state]
  (as-> state state
        (update-in state [:navigation-2d :enabled] not)
        (assoc-in state [:draw :enabled]
                  (not (get-in state [:navigation-2d :enabled])))))

(defn mouse-released [state event]
  (let [{:keys [x0 y0 x1 y1]} (state :selection)]
    (if (and x0 y0 x1 y1)
      (let [{:keys [button]} event
            {:keys [field]} state
            [x0 x1] (sort [x0 x1])
            [y0 y1] (sort [y0 y1])
            selection (select-keys
                        field
                        (for [[{:keys [x y] :as p} v] field
                              :when (and (<= x0 x x1)
                                         (<= y0 y y1))]
                          p))]
        (cond-> state
          (not (empty? selection))
          (as-> state
                (case (:last-mouse-button state)
                  :left (as-> state state
                              (assoc-in state [:draw :buffer] (center-cells selection))
                              (assoc-in state [:draw :brush] (-> state :draw :buffer)))
                  :center (as-> state state
                                (assoc-in state [:draw :buffer] (center-cells selection))
                                (assoc-in state [:draw :brush] (-> state :draw :buffer))
                                (update state :field #(apply dissoc % (keys selection))))
                  :right (update state :field #(apply dissoc % (keys selection)))))

          true
          (dissoc :selection)

          (-> state :navigation-2d :enabled)
          (toggle-nav-draw-mode)))
      state)))

(defn mouse-button-event [state event]
  (let [{:keys [x y button]} event
        coord (disp-coord->field-coord state (q/mouse-x) (q/mouse-y))]
    (if (and (q/mouse-pressed?) (q/key-pressed?) (:control (q/key-modifiers)))
      (let [coords (disp-coord->field-coord state (q/mouse-x) (q/mouse-y))]
        (-> state
            (assoc-in [:selection :x1] (:x coords))
            (assoc-in [:selection :y1] (:y coords))))
      (case button
        :left (let [offset (cells-offset coord (-> state :draw :brush))]
                (update state :field #(merge % offset)))
        :right (update state :field #(dissoc % coord))
        :center (if (:paused state)
                  (let [{x0 :x, y0 :y} (disp-coord->field-coord state 0 0)
                        {x1 :x, y1 :y} (disp-coord->field-coord state (q/width) (q/height))]
                    (cond-> state
                            (-> state :navigation-2d :enabled) (toggle-nav-draw-mode)
                            true (assoc :field (random-rgb-cells [x0 x1] [y0 y1]))))
                  (as-> state state
                        (update state :paused not)
                        (if (= (:paused state) (get-in state [:navigation-2d :enabled]))
                          (toggle-nav-draw-mode state)
                          state)
                        (assoc state :field {})
                        (assoc state :generation 0)))
        state))))

(defn key-pressed [state event]
  (let [{:keys [key-code key]} event]
    (case (get-in state [:key-bindings key-code] :no-binding)
      :pause
      (as-> state state
            (update state :paused not)
            (if (= (:paused state) (get-in state [:navigation-2d :enabled]))
              (toggle-nav-draw-mode state)
              state))

      :dec-update-rate
      (update state :update-rate (limited 1 40 dec))

      :inc-update-rate
      (update state :update-rate (limited 1 40 inc))

      :toggle-grid
      (update-in state [:grid :enabled] not)

      :toogle-nav-draw-mode
      (toggle-nav-draw-mode state)

      :reset-brush
      (-> state
          (assoc-in [:draw :brush] {{:x 0 :y 0} [255 255 255]})
          (assoc-in [:draw :buffer] {{:x 0 :y 0} [255 255 255]}))

      :toggle-show-frame-rate
      (let [{:keys [update-rate target-frame-rate]} (state :show-info)]
        (if (= update-rate target-frame-rate)
          (update-in state [:show-info :update-rate] not)
          (update-in state [:show-info :target-frame-rate] not)))

      :toggle-show-generation
      (update-in state [:show-info :generation] not)

      :toggle-show-delta
      (update-in state [:show-info :delta] not)

      :toggle-show-zoom
      (update-in state [:show-info :zoom] not)

      :reset-camera
      (update state :navigation-2d #(merge % (:reset-navigation state)))

      :no-binding
      state)))

(defn mouse-wheel [state dir]
  (as-> state state
        (update-in state [:draw :pick]
                   #(mod (- % dir) (count (get-in state [:draw :palette]))))
        (let [pick (-> state :draw :pick)
              coord (disp-coord->field-coord state (q/mouse-x) (q/mouse-y))
              col (get-in state [:field coord] (get-in state [:draw :palette pick]))]
          (cond-> state
                  (= pick 1)
                  (assoc-in [:draw :palette pick] col)

                  (not (zero? pick))
                  (update-in [:draw :brush]
                             (partial
                               recolor-cells
                               (get-in state [:draw
                                              :palette
                                              (get-in state [:draw :pick])])))

                  (zero? pick)
                  (assoc-in [:draw :brush] (-> state :draw :buffer))))))

(defn -main [& args]
  (q/defsketch
    life
    :host "life"
    :title "Game of Life"
    :setup setup
    :size [800 450]
    :features [:resizable]
    :update update-state
    :draw draw-state
    :mouse-pressed mouse-pressed
    :mouse-clicked mouse-button-event
    :mouse-dragged mouse-button-event
    :mouse-released mouse-released
    :mouse-wheel mouse-wheel
    :key-pressed key-pressed
    :info-provider info-provider
    :middleware [m/fun-mode
                 m/pause-on-error
                 ;record-gif
                 show-info
                 navigation-2d]))
# game-of-life

Clojure implementation of Conway's Game of Life.

## Usage:

Just `lein run` it or build jar with leiningen

Edit "settings.edn" to change settings.

Controls:

    toggle pause                          space
    toogle mode                           D

    increase update rate                  >
    decrease update rate                  <

    reset brush                           E
    reset camera                          R

    toogle grid rendering                 C
    toggle generation displaying          G
    toggle frame rate displaying          F
    toggle camera position displaying     H
    toggle camera zoom displaying         J

    Selection:
        copy                              ctrl+LMB
        delete                            ctrl+RMB
        cut                               ctrl+CMB

    Draw mode:                      
        draw                              LMB
        clear field                       CMB while action
        fill field with random cells      CMB on pause
        chage colors*                     mouse wheel
        pick color                        swhitch to picked color
                                          while pointer is over
                                          colored cell

    Navigation mode:
        move camera                       drag mouse
        control zoom                      mouse wheel

*colors pallete is 
[white, red, green, blue, default brush color, picked color]

## License

Copyright � 2017

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.